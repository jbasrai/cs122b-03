import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;


public class Main {
	
	private static Scanner scanner = new Scanner(System.in);
	private static Connection connection = null;
	
	public static void main(String[] args) throws Exception {
		login();
		while(true) {
			printMenu();
			int input = getValidMenuInput();
			
			switch(input) {
				case 1:
					showGrants();
				case 0:
					exit();
			}
		}
	}
	
	public static void login() throws Exception {
		Class.forName("com.mysql.jdbc.Driver").newInstance();

		while(true) {
			try {
				String user = "root";
				String password ="1ndoviet";
				connection = DriverManager.getConnection("jdbc:mysql:///moviedb", user, password);
				break;
			}
			catch(Exception e) {
				System.out.println("\nInvalid login credentials. Please try again.\n");
			}
		}
		
		System.out.println("\nLogin successful! Connected to database.\n");
	}
	
	public static void printMenu() {
		System.out.println("MENU: ");
		System.out.println("1. Show grants");
		System.out.println("0. Exit the program\n");
	}
	
	public static int getValidMenuInput() {
		int input = -1;
		while(true) {
			try {
				String next = scanner.nextLine();
				input = Integer.parseInt(next);
				if(input < 0 || input > 8) {
					throw new Exception();
				}
				break;
			}
			catch(Exception e) {
				System.out.println("\nInvalid input. Please enter a number between 1-8.\n");
				printMenu();
			}
		}
		return input;
	}
	
	public static void showGrants() {
		
	}
	
	public static void exit() {
		System.out.println("Program closed");
		System.exit(0);
	}
}
